<?php
/**
 * The template for displaying individual use cases
 */

get_header(); 
?>

<!-- OPENING SECTION -->
<section class="opening">
  <div class="container-fluid">
    <div class="row align-items-center">

      <!-- Feature image -->
      <div class="col-md-4 px-0 ">
        <figure>
          <div class="opening__use-case-image-container">
            <img class="img-fluid opening__image" src="<?php esc_url(the_post_thumbnail_url()); ?>" alt="">
            <figcaption class="text-right"><?php esc_html(the_post_thumbnail_caption()); ?></figcaption>
          </div>
        </figure>
      </div>

      <div class="col-sm-8">
      
      <!-- Main heading -->
        <div class="row mb-lg-5">
          <div class="col-md-10 col-sm-12">
            <h1 class="heading-primary"><?php esc_html(the_title()); ?></h1>
            <p class="text-medium"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_opening_summary', true)); ?></p>
            <a class="opening__download-text" href="<?php echo esc_url(get_item_from_group('_use_cases_cta', 'cta_url')); ?>">
              <span class=><?php echo esc_html(get_item_from_group('_use_cases_cta', 'cta_text')); ?></span>
              <i class="fas fa-arrow-right"></i>
            </a>
          </div>
        </div>

        <!-- Bottom border -->
        <div class="row">
          <div class="col-12 pl-0 mb-md-3 md-lg-0">
            <div class="opening__border-bottom"></div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</section>

<!-- USE CASE SUMMARY SECTION -->
<section class="use-case-summary">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-lg-6 offset-sm-2">

        <!-- USE CASE SUMMARY DETAILS -->
        <div class="use-case-summary__details mb-5">
          <h2 class="heading-secondary"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_subheading', true)); ?></h2>
          <p><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_subheading_summary', true)); ?></p>
        </div>

        <!-- USE CASE BENEFITS -->
        <div class="use-case-summary__benefits">

          <h3 class="heading-tertiary mb-3"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_benefit_heading', true)); ?></h3>

          <?php output_use_case_benefits(); ?>
          
        </div>

        <!-- USE CASE CLOSING SUMMARY -->
        <p class="heading-tertiary"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_closing_summary', true)); ?></p>
        
        <div>
          <a href="<?php echo esc_url(get_item_from_group('_use_cases_cta', 'cta_url')); ?>" class="btn button button--green button--large"><?php echo esc_html(get_item_from_group('_use_cases_cta', 'cta_text')); ?></a>
        </div>
      </div>
    </div>

    <!-- SECONDARY IMAGE -->

      <div class="row">
        <div class="col-sm-10 offset-sm-1">
        <figure>
          <div class="technical-resources__secondary-image-container  my-5">
            <img class="img-fluid" src="<?php echo esc_url(get_item_from_group('_use_cases_secondary_image', 'secondary_image')); ?>" alt="Use Case Image">
            <figcaption class="text-right"><?php echo esc_html(get_item_from_group('_use_cases_secondary_image', 'secondary_image_caption')); ?></figcaption>
          </div>
        </figure>
        </div>
      </div>

      <!-- TECHNICAL RESOURCES -->

    <!-- This section defaults to display: none. d-none class removed with JavaScript if at least one product sheet is uploaded -->
    <section class="technical-resources d-none">
      <div class="row">
        <div class="col-sm-10 offset-sm-1">
          <h2 class="heading-secondary mb-4"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_products_heading', true)); ?></h2>
          <p class="mb-5 color-black"><?php echo esc_html(get_post_meta(get_the_ID(), '_use_cases_products_subheading', true)); ?></p>
        </div>
      </div>

      <div class="row">
        <!-- <div class="col-sm-12 col-md-6 offset-md-1">
          <div class="d-flex flex-wrap"> -->
        
          <?php output_use_case_product_sheets(); ?>
          
          <!-- </div> -->
        <!-- </div> -->
      </div>
    </section>

  </div> <!-- END CONTAINER -->
</section>


  <!-- CONTACT US SECTION -->
  <section class="contact contact--bg-grey">
    <div class="container">
      <div class="row contact__main">
        <div class="col-sm-9 offset-sm-1">
          
          <h3 class="contact__heading heading-secondary color-light-green"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_use_case_contact_heading', true)); ?></h3>
          
          <p class="text-medium"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_use_case_contact_description', true)); ?></p>
          
          <p class="text-medium text-bold"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_use_case_contact_closing', true)); ?></p>
          
          <a href="<?php echo site_url('/contact-us'); ?>" class="btn button button--green button--large">Contact Us</a>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-8 contact__border-dark"></div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>