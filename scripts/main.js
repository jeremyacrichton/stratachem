/**********************************
 * Custom scripts for our theme
 **********************************/

$(document).ready(function() {
  /**
   * Add 'active' class to first carousel item after document loads
   */
  if ($('.carousel-item')) {
    $('.carousel-item')
      .first()
      .addClass('active');
  }

  /**
   * Show technical resources section if at least 1 product has been added
   */
  if ($('.technical-resources__product-sheet').length > 0) {
    $('.technical-resources').removeClass('d-none');
  }
});
