<?php

 /**********************************
 * Functions and definitions for our theme
 **********************************/

 // Include metaboxe setup file
include_once dirname(__FILE__) . '/inc/metaboxes-use-cases.php';
include_once dirname(__FILE__) . '/inc/metaboxes-front-page.php';
include_once dirname(__FILE__) . '/inc/metaboxes-contact-us.php';

/**
 * Enqueue scripts & stylesheets
 */
function stratachem_files() {

  // Bootstrap Scripts
  wp_enqueue_script('bootstrap-scripts', get_theme_file_uri('/scripts/bootstrap.min.js'), NULL, microtime(), true);

  // jQuery Scripts
  wp_enqueue_script('jquery-scripts', get_theme_file_uri('/scripts/jquery.min.js'), NULL, microtime(), true);

  // Popper Scripts
  wp_enqueue_script('popper-scripts', get_theme_file_uri('/scripts/popper.min.js'), NULL, microtime(), true);

  // Custom Scripts
  wp_enqueue_script('custom-scripts', get_theme_file_uri('/scripts/main.js'), NULL, microtime(), true);

  // Google Fonts
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Titillium+Web:300,400,700,900');
  
  // Font CSS CDN
  wp_enqueue_style('fontawesome-styles', '//use.fontawesome.com/releases/v5.3.1/css/all.css');
  
  // Bootstrap CSS
  wp_enqueue_style('bootstrap-styles', get_template_directory_uri() . '/css/bootstrap.min.css');

  // Custom CSS
  wp_enqueue_style('stratachem-styles', get_template_directory_uri() . '/css/styles.css');
}
add_action('wp_enqueue_scripts', 'stratachem_files');

/**
 * Register support for various WordPress features
 */
function stratachem_features() {
  // Register main naviation menu
  register_nav_menu('top', 'Top Menu');
  // Allow theme to add document title tag to HTML <head>
  add_theme_support('title-tag');
  // Enable thumbnails
  add_theme_support('post-thumbnails');
  // Enable custom logo
  add_theme_support('custom-logo', array(
    'height' => 98,
    'width' => 268
  ));
}
add_action('after_setup_theme', 'stratachem_features');

/**
 * Register Custom Navigation Walker
 */
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


/**********************************
 * OTHER FUNCTION DEFINITIONS
 **********************************/

/**
 * Return single field from group metabox
 */
function get_item_from_group($group_id, $item_id) {
  $entries = get_post_meta(get_the_ID(), $group_id, true);

  foreach((array) $entries as $key => $entry) {
    if (isset($entry[$item_id])) {
      return $entry[$item_id];
    }
  }  
}

/**
 * Output use case benefits
 */
function output_use_case_benefits() {
  $items = get_post_meta( get_the_ID(), '_use_cases_benefits', true );
  
  if ($items) {
    foreach ($items as $key => $item) {
      echo '<div class="row use-case-summary__benefits-item align-items-center">';
      echo '<div class="col-2">';
      echo '<i class="far fa-check-square use-case-summary__benefits-checkbox"></i>';
      echo '</div>';
      echo '<div class="col-10">';
      echo '<span class="color-black">' . $item . '</span>';
      echo '</div>';
      echo '</div>';
    }
  }
}

/**
 * Output use case product sheets in single-use-case.php
 */
function output_use_case_product_sheets() {
  $entries = get_post_meta( get_the_ID(), '_use_cases_products', true);

  foreach ( (array) $entries as $key => $entry) {
    $name = '';
    $sheet = '';

    if (isset($entry['_use_cases_product_name'])) {
      $name = esc_html($entry['_use_cases_product_name']);
    }

    if (isset($entry['_use_cases_product_sheet'])) {
      $sheet = esc_url($entry['_use_cases_product_sheet']);
    }

    if ($entries) {
      echo '<div class="col-lg-4 col-md-5 offset-sm-1">';
      echo '<div class="technical-resources__product-sheet mb-4">';
      echo '<i class="far fa-file-pdf"></i>';
      echo '<a href="' . $sheet . '"><span class="pl-2">' . $name . '</span></a>';
      echo '<i class="fas fa-arrow-right pl-5"></i>';
      echo '</div>';
      echo '</div>';
    }
  }
}

/**
 * Output testimonials in carousel in front-page.php
 */
function output_testimonials() {
  $entries = get_post_meta( get_the_ID(), '_front_page_testimonials', true);

  foreach ((array) $entries as $key => $entry) {
    $testimonial = '';
    $name = '';

    if (isset($entry['testimonial'])) {
      $testimonial = esc_html($entry['testimonial']);
    }

    if (isset($entry['testimonial_by'])) {
      $name = esc_html($entry['testimonial_by']);
    }

    // Note, active class is added to our first carousel-item in main.js
    echo '<div class="carousel-item testimonials__carousel-item">';
    echo '<div class="row">';
    echo '<div class="col-10 col-sm-9">';
    echo '<div class="testimonials__description mb-3">' . $testimonial . '</div>';
    echo '<div class="testimonials__name">' . $name . '</div>';
    echo '</div>';
    echo '</div>';
    echo '</div>';

  };
}

/**
 * Output services in front-page.php
 */
function output_stratachem_services() {
  $entries = get_post_meta( get_the_ID(), '_front_page_services', true);
  
  foreach ( (array) $entries as $key => $entry) {
    $name = '';
    $description = '';
    $learn_more_text = '';
    $learn_more = '';

    if (isset($entry['service_name'])) {
      $name = esc_html($entry['service_name']);
    }

    if (isset($entry['service_description'])) {
      $description = esc_html($entry['service_description']);
    }

    if (isset($entry['service_learn_more_text'])) {
      $learn_more_text = esc_html($entry['service_learn_more_text']);
    }

    if (isset($entry['service_learn_more'])) {
      $learn_more = esc_url($entry['service_learn_more']);
    }

    echo '<div class="col-md-6 services__service-box">';
    echo '<h3 class="heading-tertiary">' . $name . '</h3>';
    echo '<p class="services__description">' . $description .'</p>';
    echo '<a href="' . $learn_more . '" class="btn btn-outline-success button--green">' . $learn_more_text . '</a>';
    echo '</div>';
  }
}

/**
 * Output training services in front-page.php
 */

function output_training_services() {
  $entries = get_post_meta( get_the_ID(), '_front_page_training', true);
  $count = 1;
  
  foreach ( (array) $entries as $key => $entry) {
    $service = '';
    $description = '';
    

    if (isset($entry['service_title'])) {
      $service = esc_html($entry['service_title']);
    }

    if (isset($entry['service_description'])) {
      $description = esc_html($entry['service_description']);
    }

    echo '<div class="row mb-4  training__services">';
    echo '<div class="col-1 offset-sm-2">';
    echo '<h2>' . $count . '</h2>';
    echo '</div>';
    echo '<div class="col-8 col-sm-4">';
    echo '<div class="row training__services__heading">' . $service . '</div>';
    echo '<div class="row  training__services__description">' .  $description . '</div>';
    echo '</div>';
    echo '</div>';

    $count++;
  }
}

/**
 * Output featured products cards in front-page.php
 */
function output_featured_products_cards() {
  $entries = get_post_meta( get_the_ID(), '_front_page_featured_products', true);

  foreach((array) $entries as $key => $entry) {
    $name = '';
    $description = '';
    $sheet = '';
    $image = '';

    if (isset($entry['product_name'])) {
      $name = esc_html($entry['product_name']);
    }

    if (isset($entry['product_description'])) {
      $description = esc_html($entry['product_description']);
    }

    if (isset($entry['product_sheet'])) {
      $sheet = esc_html($entry['product_sheet']);
    }

    if (isset($entry['product_image'])) {
      $image = esc_html($entry['product_image']);
    }

    echo '<div class="col-sm-6 col-md-3 mb-5">';
    echo '<div class="featured-products__image mx-auto mx-md-0">';
    echo '<img src="' . $image .'" alt="Terrex">';
    echo '</div>';
    echo '<div class="col-xl-9 pl-0">';
    echo '<div class="featured-products__subheading">' . $name .'</div>';
    echo '<p class="featured-products__description">' . $description .'</p>';
    echo '<a href="' . $sheet .'" class="featured-products__link">View product sheet</a>';
    echo '</div>';
    echo '</div>';
  }
}

/**
 * Output address in contact us page
 */

function output_contact_address() {
  $entries = get_post_meta( get_the_ID(), '_contact_us_address', true);

  if ($entries) {
    foreach ((array) $entries as $key => $entry) {
      echo '<div class="contact__text">' . esc_html($entry) . '</div>';
    }
  }
}

?>