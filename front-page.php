<?php
/**
 * The template for displaying the front page
 */

 get_header();

 ?>

 <!-- OPENING SECTION -->
 <section class="opening">
    <div class="container-fluid">
      <div class="row align-items-center">

        <!-- Feature image -->
        <div class="col-4 px-0">
          <div class="opening__front-page-image-container">
            <img class="image-fluid opening__image" src="<?php esc_url(the_post_thumbnail_url()); ?>" alt="Wood Industry">
          </div>
        </div>

        <div class="col-8">

          <!-- Main heading -->
          <div class="row">
            <div class="col-lg-5 col-md-8 ml-sm-5">
              <h1 class="heading-primary"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_main_heading', true)); ?></h1>
            </div>
          </div>

          <!-- Bottom border -->
          <div class="row">
            <div class="col-12 pl-0">
              <div class="opening__border-bottom"></div>
            </div>
          </div>
          
        </div>
      </div>
    </div>

    <div class="container-fluid opening__why-choose-us-container">
      <div class="row align-items-center">
        <div class="col-sm-2 offset-2">
          
        <!-- Square shape -->
          <div class="opening__square-container">
            <div class="opening__square"></div>
          </div>
        </div>

        <div class="col-lg-4 ml-sm-5 col-md-6 opening__why-choose-us">
          <h2 class="heading-secondary color-black"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_secondary_heading', true)); ?></h2>
          <p class="opening__why-choose-us__description">
            <?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_secondary_description', true)); ?>
          </p>
        </div>
      </div>
    </div>
  </section>

  <!-- Secondary image and caption -->
  <div class="container mt-5">
    <div class="row">
      <div class="col-sm-10 offset-sm-2">
        <figure>
          <div class="opening__front-page-image-secondary-container">
            <img class="img-fluid opening__image-2" src="<?php echo esc_url(get_item_from_group('_front_page_secondary_image', 'secondary_image')); ?>" alt="">  
            <figcaption class="text-right"><?php echo esc_html(get_item_from_group('_front_page_secondary_image', 'secondary_image_caption')); ?></figcaption>
          </div>
        </figure>
      </div>
    </div>
  </div>

  <!-- TESTIMONIALS SECTION -->
  <section class="testimonials">
    <div class="container">
      
      <div class="row">
        <div class="col-sm-10 offset-sm-2">
          <h2 class="testimonials__heading heading-secondary"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_testimonials_heading', true)); ?></h2>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-10 col-lg-9 offset-sm-2">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            
            <div class="carousel-inner">
              <?php output_testimonials(); ?>
            </div>

            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon"></span>
            </a>
          </div>
        </div>
      </div>
      
    </div>
  </section>

  <!-- Green border from end to end -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="testimonials__border-bottom"></div>
      </div>
    </div>
  </div>


  <!-- SERVICES SECTION -->
  <section class="services">
    <div class="container">

      <div class="row">
        <div class="col-md-10 offset-md-2 mb-5">
          <h2 class="heading-secondary"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_services_heading', true)); ?></h2>
        </div>
      </div>

      <div class="row services__row">
        <div class="col-md-10 offset-md-2">
          <div class="row">
            <?php output_stratachem_services(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- CONTACT US SECTION -->
  <section class="contact contact--bg-green color-white">
    <div class="container-fluid">
      <div class="row">
        <div class="col-9 contact__border-light"></div>
      </div>
    </div>

    <div class="container contact__main">
      <div class="row py-5">
        <div class="col-sm-10 col-lg-6 ml-sm-0">
          <h3 class="contact__heading"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_contact_heading', true)); ?></h3>
        </div>
      </div>

      <div class="row">
        <div class="col-10 ml-4 ml-sm-0">
          <a href="<?php echo esc_url(site_url('/contact-us')); ?>" class="btn btn-outline-light button button--large"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_contact_cta_text', true)); ?></a>
        </div>
      </div>

    </div>
  </section>

  <!-- TRAINING SECTION -->
  <section class="training">
    <div class="container">
      <div class="row">
        <div class="col-sm-7 mt-5 mb-5">
          <figure>
            <div class="training__image-container">
              <img class="img-fluid" src="<?php echo esc_url(get_item_from_group('_front_page_third_image', 'third_image')); ?>" alt="Training and Consulting">
              <figcaption class="text-right"><?php echo esc_html(get_item_from_group('_front_page_third_image', 'third_image_caption')); ?></figcaption>
            </div>
          </figure>
        </div>
      </div>

      <div class="row">
        <div class="col-10 col-sm-6 col-lg-4 offset-sm-2">
          <h4 class="training__heading heading-secondary"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_training_heading', true)); ?></h4>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8 offset-sm-2">
          <h5 class="training__subheading text-medium"><?php echo esc_html(get_post_meta(get_the_ID(), '_front_page_training_subheading', true)); ?></h5>
        </div>
      </div>

      <?php output_training_services(); ?>

      <!-- Training and Consulting CTA -->
      <div class="row">
        <div class="col offset-sm-2">
          <a href="<?php echo esc_url(get_item_from_group('_front_page_training_cta', 'cta_url')); ?>" class="btn btn-outline-success button--green text-small"><?php echo esc_html(get_item_from_group('_front_page_training_cta', 'cta_text')); ?></a>
        </div>
      </div>
    </div>
  </section>

  <!-- FEATURED PRODUCTS SECTION -->
  <section class="featured-products mt-5">
    <div class="container">
      <h4 class="featured-products__heading heading-secondary">Featured Products</h4>

      <div class="row text-center text-md-left">
        <?php output_featured_products_cards(); ?>
      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-9 featured-products__border-bottom"></div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>