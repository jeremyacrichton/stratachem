<?php
/* Template Name: Contact Us Template */

/**
 * The template for displaying the contact us page
 */

get_header();

?>

<!-- OPENING SECTION -->

<section class="opening">
  <div class="container-fluid">
    <div class="row align-items-center">

      <!-- Feature Image -->
      <div class="col-sm-4 px-0">
        <div class="opening__contact-us-image-container">
          <img class="img-fluid opening__image" src="<?php esc_url(the_post_thumbnail_url()); ?>" alt="">
        </div>
      </div>

      <div class="col-sm-8">
        
        <!-- Main heading -->
        <div class="row mb-5">
          <div class="col-md-8 col-sm-12 offset-sm-1">
            <h1 class="heading-primary"><?php echo esc_html(get_post_meta(get_the_ID(), '_contact_us_main_heading', true)); ?></h1>
            <p class="text-medium"><?php echo esc_html(get_post_meta(get_the_ID(), '_contact_us_main_description', true)); ?></p>
          </div>
        </div>

        <!-- Bottom border -->
        <div class="row">
          <div class="col-sm-10 pl-0">
            <div class="opening__border-bottom"></div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<!-- CONTACT FORM SECTION -->
<section class="contact-form">
  <div class="container">
      <div class="row mb-4">
        <div class="col-sm-6 col-md-5 offset-md-1">
          <div class="heading-secondary">Message us</div>
        </div>
    </div>
  </div>

  <?php
    while(have_posts()) {
      the_post(); ?>
      <?php the_content(); ?>
    <?php }
  ?>

</section>

<!-- CONTACT INFO SECTION -->

<section class="contact contact--bg-grey">
  <div class="container contact__main">
    <div class="row">
      <div class="col-sm-6 col-md-5 offset-md-1">
        <h4 class="heading-green mb-4">Give us a call:</h4>
        <div>
          <span class="contact__text contact__text--bold">Office:</span>
          <span class="contact__text"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_office_phone', true)); ?></span>
        </div>
        <div>
          <span class="contact__text contact__text--bold">Toll Free Sales:</span>
          <span class="contact__text"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_tollfree_phone', true)); ?></span>
        </div>
        <div>
          <span class="contact__text contact__text--bold">Toll Free Admin:</span>
          <span class="contact__text"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_tollfree_admin', true)); ?></span>
        </div>
        <div>
          <span class="contact__text contact__text--bold">Fax:</span>
          <span class="contact__text"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_fax', true)); ?></span>
        </div>
      </div>

      <div class="col-sm-6 col-md-5 offset-md-1 mt-5 mt-sm-0">
        <h4 class="heading-green mb-4">Mailing address:</h4>
        <address>
          <div class="contact__text"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_name', true)); ?></div>
          <?php output_contact_address(); ?>
      </div>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="col-8 contact__border-dark"></div>
    </div>
  </div>
</section>




<?php get_footer(); ?>