<?php

/**********************************
 * Define CMB2 metabox and field configurations - Contact Us
 **********************************/

function metaboxes_contact_us() {
  /**
	 * Initiate the metabox for Contact Us page
	 */

  $prefix_contact_us = '_contact_us_';

	$cmb_contact_us = new_cmb2_box( array(
		'id'            => 'contact_us_metabox',
		'title'         => __( 'Contact Us Metabox', 'cmb2' ),
    'object_types'  => array( 'page', ), // post type
    'show_on'      => array( 'key' => 'page-template', 'value' => 'template-contact.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true,
  ));

  // Contact us heading
  $cmb_contact_us->add_field( array(
    'name'       => __( 'Main Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_contact_us . 'main_heading',
		'type'       => 'text',
  ));

  // Contact us description
  $cmb_contact_us->add_field( array(
    'name'       => __( 'Main Description', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_contact_us . 'main_description',
		'type'       => 'textarea',
  ));

  // Address
  $cmb_contact_us->add_field( array(
    'name'    => 'Address',
    'default' => '',
    'id'         => $prefix_contact_us . 'address',
    'type'    => 'text_medium',
    'repeatable' => true,
  ));
}
add_action( 'cmb2_admin_init', 'metaboxes_contact_us' );
?>