<?php

 /**********************************
 * Define CMB2 metabox and field configuration - Use Cases
 **********************************/

function metaboxes_use_cases() {
	// Start with an underscore to hide fields from custom fields list
	$prefix = '_use_cases_';

	/**
	 * Initiate the metabox for Use Cases
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'use_cases_metabox',
		'title'         => __( 'Use Cases Metabox', 'cmb2' ),
		'object_types'  => array( 'use-case', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
  ));

	// Opening Summary
	$cmb->add_field( array(
    'name'       => __( 'Opening Summary', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix . 'opening_summary',
		'type'       => 'textarea',
  ));

  // CTA group
  $use_case_cta = $cmb->add_field( array(
    'id'          => $prefix . 'cta',
    'type'        => 'group',
    'description' => __( 'Use Case CTA', 'cmb2' ),
    'repeatable' => false,
  ));
    
  // CTA text
  $cmb->add_group_field( $use_case_cta, array(
    'name'    => 'Use Case CTA text',
    'default' => '',
    'id'         => 'cta_text',
    'type'    => 'text',
  ));

  // CTA URL
  $cmb->add_group_field( $use_case_cta, array(
    'name'    => 'Use Case CTA URL',
    'default' => '',
    'id'         => 'cta_url',
    'type'    => 'text',
  ));

  
  // Subheading
  $cmb->add_field( array(
    'name'    => 'Subheading',
    'default' => '',
    'id'      => $prefix . 'subheading',
    'type'    => 'text_medium'
  ));

	// Subheading Summary
	$cmb->add_field( array(
    'name'       => 'Subheading Summary',
    'default'    => '',
		'id'         => $prefix . 'subheading_summary',
		'type'       => 'textarea',
  ));

  // Benefit Heading
  $cmb->add_field( array(
    'name'    => 'Benefit Heading',
    'default' => '',
    'id'         => $prefix . 'benefit_heading',
    'type'    => 'text_medium'
  ));

  // Benefits
  $cmb->add_field( array(
    'name'    => 'Benefits',
    'default' => '',
    'id'         => $prefix . 'benefits',
    'type'    => 'text_medium',
    'repeatable' => true,
  ));

	// Closing Summary
	$cmb->add_field( array(
    'name'       => 'Closing Summary',
    'default'    => '',
		'id'         => $prefix . 'closing_summary',
		'type'       => 'textarea',
  ));

  // Secondary image group
  $secondary_image = $cmb->add_field( array(
    'id'          => $prefix . 'secondary_image',
    'type'        => 'group',
    'description' => __( 'Secondary Image', 'cmb2' ),
    'repeatable'  => false,
    'options'     => array(
      'group_title'   => __( 'Secondary Image', 'cmb2' ),
      'add_button'    => __( 'Add an Image', 'cmb2' ),
      'remove_button' => __( 'Remove File', 'cmb2' ),
      'sortable'      => true,
    ),
  ));

  // Secondary image group - image
  $cmb->add_group_field( $secondary_image, array(
    'name'    => 'Secondary Image',
    'id'      => 'secondary_image',
    'type'    => 'file',
    'options' => array(
      'url' => false, 
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image File'
    ),
    'query_args' => array(
      'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
      ),
    ),
    'preview_size' => 'large',
  ));

  // Secondary image group - caption
  $cmb->add_group_field( $secondary_image, array(
    'name'    => 'Secondary Image Caption',
    'default' => '',
    'id'         => 'secondary_image_caption',
    'type'    => 'text',
  ));

  // Products heading
	$cmb->add_field( array(
    'name'       => __( 'Products Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix . 'products_heading',
		'type'       => 'text',
  ));

  // Products subheading
	$cmb->add_field( array(
    'name'       => __( 'Products Subheading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix . 'products_subheading',
		'type'       => 'text',
  ));

  // Products Group
  $products = $cmb->add_field( array(
    'id'          => $prefix . 'products',
    'type'        => 'group',
    'description' => __( 'Products', 'cmb2' ),
    'options'     => array(
      'group_title'   => __( 'Product {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
      'add_button'    => __( 'Add Another Product', 'cmb2' ),
      'remove_button' => __( 'Remove Product', 'cmb2' ),
      'sortable'      => true, // beta
      // 'closed'     => true, // true to have the groups closed by default
    ),
  ));

  // Product name
  $cmb->add_group_field( $products, array(
    'name'    => 'Product Name',
    'default' => '',
    'id'         => $prefix . 'product_name',
    'type'    => 'text_medium',
  ));

  // Product sheet
  $cmb->add_group_field( $products, array(
    'name'    => 'Product Sheet',
    'desc'    => 'Upload a pdf.',
    'id'      => $prefix . 'product_sheet',
    'type'    => 'file',
    'options' => array(
      'url' => false,
    ),
    'query_args' => array(
      'type' => 'application/pdf', // Make library only display PDFs.
    ),
    'preview_size' => 'large',
  ));
  
}
add_action( 'cmb2_admin_init', 'metaboxes_use_cases' );

?>