<?php

/**********************************
 * Define CMB2 metabox and field configurations - Front Page
 **********************************/

function metaboxes_front_page() {
  /**
	 * Initiate the metabox for Front Page
	 */

  $prefix_front_page = '_front_page_';

	$cmb_front_page = new_cmb2_box( array(
		'id'            => 'front_page_metabox',
		'title'         => __( 'Front Page Metabox', 'cmb2' ),
    'object_types'  => array( 'page', ), // post type
    'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true,
  ));

  // Include metabox on front page
  function metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
      return $display;
    }
  
    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
      return $display;
    }
  
    $post_id = 0;
  
    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
      $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
      $post_id = $_POST['post_ID'];
    }
  
    if ( ! $post_id ) {
      return false;
    }
  
    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );
  
    // there is a front page set and we're on it!
    return $post_id == $front_page;
  }
  add_filter( 'cmb2_show_on', 'metabox_include_front_page', 10, 2 );

  // Phone number
  $cmb_front_page->add_field( array(
    'name'       => __( 'Phone', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'phone',
		'type'       => 'text',
  ));

  // Email
  $cmb_front_page->add_field( array(
    'name'       => __( 'Email', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'email',
		'type'       => 'text',
  ));

  // Main heading
	$cmb_front_page->add_field( array(
    'name'       => __( 'Main Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'main_heading',
		'type'       => 'textarea',
  ));

  // Secondary heading
	$cmb_front_page->add_field( array(
    'name'       => __( 'Secondary Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'secondary_heading',
		'type'       => 'text_medium',
  ));

  // Secondary description
	$cmb_front_page->add_field( array(
    'name'       => __( 'Secondary Description', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'secondary_description',
		'type'       => 'textarea',
  ));
    

  // Secondary Image group
  $secondary_image = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'secondary_image',
    'type'        => 'group',
    'description' => __( 'Secondary Image', 'cmb2' ),
    'repeatable'  => false,
    'options'     => array(
      'group_title'   => __( 'Secondary Image', 'cmb2' ),
      'add_button'    => __( 'Add an Image', 'cmb2' ),
      'remove_button' => __( 'Remove File', 'cmb2' ),
      'sortable'      => true,
    ),
  ));

  // Secondary Image group - image
  $cmb_front_page->add_group_field( $secondary_image, array(
    'name'    => 'Secondary Image',
    'id'      => 'secondary_image',
    'type'    => 'file',
    'options' => array(
      'url' => false, 
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image File'
    ),
    'query_args' => array(
      'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
      ),
    ),
    'preview_size' => 'large',
  ));

  // Secondary Image Caption
  $cmb_front_page->add_group_field( $secondary_image, array(
    'name'    => 'Secondary Image Caption',
    'default' => '',
    'id'         => 'secondary_image_caption',
    'type'    => 'text_medium',
  ));

  // Testimonials heading
	$cmb_front_page->add_field( array(
    'name'       => __( 'Testimonials Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'testimonials_heading',
		'type'       => 'text_medium',
  ));

  // Testimonials group
  $testimonials = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'testimonials',
    'type'        => 'group',
    'description' => __( 'Testimonials', 'cmb2' ),
    'options'     => array(
      'group_title'   => __( 'Testimonial {#}', 'cmb2' ),
      'add_button'    => __( 'Add Another Testimonial', 'cmb2' ),
      'sortable'      => true,
    ),
  ));
  $cmb_front_page->add_group_field( $testimonials, array(
    'name'    => 'Testimonial',
    'default' => '',
    'id'         => 'testimonial',
    'type'    => 'textarea',
  ));
  $cmb_front_page->add_group_field( $testimonials, array(
    'name'    => 'Testimonial by',
    'default' => '',
    'id'         => 'testimonial_by',
    'type'    => 'text_medium',
  ));

  // Services heading
	$cmb_front_page->add_field( array(
    'name'       => __( 'Services Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'services_heading',
		'type'       => 'text_medium',
  ));

  // Services Group
  $services = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'services',
    'type'        => 'group',
    'description' => __( 'Services', 'cmb2' ),
    'options'     => array(
      'group_title'   => __( 'Service {#}', 'cmb2' ),
      'add_button'    => __( 'Add Another Service', 'cmb2' ),
      'sortable'      => true,
    ),
  ));
    
  // Service name
  $cmb_front_page->add_group_field( $services, array(
    'name'    => 'Service Name',
    'default' => '',
    'id'         => 'service_name',
    'type'    => 'text_medium',
  ));

  // Service description
  $cmb_front_page->add_group_field( $services, array(
    'name'    => 'Service Description',
    'default' => '',
    'id'         => 'service_description',
    'type'    => 'textarea',
  ));

  // Service learn more text
  $cmb_front_page->add_group_field( $services, array(
    'name'    => 'Service Learn More text',
    'default' => '',
    'id'         => 'service_learn_more_text',
    'type'    => 'text',
  ));
  
  // Service learn more URL
  $cmb_front_page->add_group_field( $services, array(
    'name'    => 'Service Learn More URL',
    'default' => '',
    'id'         => 'service_learn_more',
    'type'    => 'text',
  ));

  // Third image group
  $third_image = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'third_image',
    'type'        => 'group',
    'description' => __( 'Third Image', 'cmb2' ),
    'repeatable'  => false,
    'options'     => array(
      'group_title'   => __( 'Third Image', 'cmb2' ),
      'add_button'    => __( 'Add an Image', 'cmb2' ),
      'remove_button' => __( 'Remove File', 'cmb2' ),
      'sortable'      => true,
    ),
  ));

  // Third image group - image
  $cmb_front_page->add_group_field( $third_image, array(
    'name'    => 'Third Image',
    'id'      => 'third_image',
    'type'    => 'file',
    'options' => array(
      'url' => false, 
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image File'
    ),
    'query_args' => array(
      'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
      ),
    ),
    'preview_size' => 'large',
  ));

  // Third image group - caption
  $cmb_front_page->add_group_field( $third_image, array(
    'name'    => 'Third Image Caption',
    'default' => '',
    'id'         => 'third_image_caption',
    'type'    => 'text',
  ));

  // Contact heading
	$cmb_front_page->add_field( array(
    'name'       => __( 'Contact Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'contact_heading',
		'type'       => 'textarea',
  ));

  // Contact CTA text
	$cmb_front_page->add_field( array(
    'name'       => __( 'Contact CTA Text', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'contact_cta_text',
		'type'       => 'text',
  ));

  // Contact CTA URL
	$cmb_front_page->add_field( array(
    'name'       => __( 'Contact CTA URL', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'contact_cta_URL',
		'type'       => 'text',
  ));

  // Training and Consulting heading
  $cmb_front_page->add_field( array(
    'name'       => __( 'Training Heading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'training_heading',
		'type'       => 'text',
  ));

  // Training and Consulting subheading
  $cmb_front_page->add_field( array(
    'name'       => __( 'Training Subheading', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'training_subheading',
		'type'       => 'text',
  ));

  // Training and Consulting group
  $training = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'training',
    'type'        => 'group',
    'description' => __( 'Training and Consulting', 'cmb2' ),
    'options'     => array(
      'group_title'   => __( 'Service {#}', 'cmb2' ),
      'add_button'    => __( 'Add Another Service', 'cmb2' ),
    ),
  ));
    
  // Service title
  $cmb_front_page->add_group_field( $training, array(
    'name'    => 'Service Title',
    'default' => '',
    'id'         => 'service_title',
    'type'    => 'text_medium',
  ));

  // Service description
  $cmb_front_page->add_group_field( $training, array(
    'name'    => 'Service Description',
    'default' => '',
    'id'         => 'service_description',
    'type'    => 'text_medium',
  ));

  // Training and Consulting CTA group
  $training_cta = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'training_cta',
    'type'        => 'group',
    'description' => __( 'Training and Consulting CTA', 'cmb2' ),
    'repeatable' => false,
  ));
    
  // Training and Consulting CTA text
  $cmb_front_page->add_group_field( $training_cta, array(
    'name'    => 'Training and Consulting CTA text',
    'default' => '',
    'id'         => 'cta_text',
    'type'    => 'text',
  ));

  // Training and Consulting CTA URL
  $cmb_front_page->add_group_field( $training_cta, array(
    'name'    => 'Training and Consulting CTA URL',
    'default' => '',
    'id'         => 'cta_url',
    'type'    => 'text',
  ));

  // Featured Products group
  $featured_products = $cmb_front_page->add_field( array(
    'id'          => $prefix_front_page . 'featured_products',
    'type'        => 'group',
    'description' => __( 'Featured Products', 'cmb2' ),
    'options'     => array(
      'group_title'   => __( 'Featured Product {#}', 'cmb2' ),
      'add_button'    => __( 'Add Another Featured Product', 'cmb2' ),
      'remove_button' => __( 'Remove File', 'cmb2' ),
      'sortable'      => true, // beta
    ),
  ));
    
  // Product name
  $cmb_front_page->add_group_field( $featured_products, array(
    'name'    => 'Product Name',
    'default' => '',
    'id'         => 'product_name',
    'type'    => 'text_medium',
  ));

  // Product description
  $cmb_front_page->add_group_field( $featured_products, array(
    'name'    => 'Product Description',
    'default' => '',
    'id'         => 'product_description',
    'type'    => 'text_medium',
  ));

  // Product sheet
  $cmb_front_page->add_group_field( $featured_products, array(
    'name'    => 'Product Sheet',
    'desc'    => 'Upload a pdf.',
    'id'      => 'product_sheet',
    'type'    => 'file',
    'options' => array(
      'url' => false, 
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add File'
    ),
    'query_args' => array(
      'type' => 'application/pdf',
    ),
    'preview_size' => 'large',
  ));

  // Product image
  $cmb_front_page->add_group_field( $featured_products, array(
    'name'    => 'Product Image',
    'id'      => 'product_image',
    'type'    => 'file',
    'options' => array(
      'url' => false,
    ),
    'text'    => array(
      'add_upload_file_text' => 'Add Image File'
    ),
    'query_args' => array(
      'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
      ),
    ),
    'preview_size' => 'medium',
  ));

  // Footer about us
  $cmb_front_page->add_field( array(
    'name'       => __( 'Footer About Us', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'footer_about_us',
		'type'       => 'textarea',
  ));

  // Company name
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Name', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_name',
		'type'       => 'text',
  ));

  // Company address 1
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Address 1', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_address_1',
		'type'       => 'text',
  ));

  // Company address 2
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Address 2', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_address_2',
		'type'       => 'text',
  ));

  // Company Office Phone
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Office Phone', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_office_phone',
		'type'       => 'text',
  ));

  // Company Toll Free Phone
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Toll Free Phone', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_tollfree_phone',
		'type'       => 'text',
  ));

  // Company Toll Free Admin
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Toll Free Admin', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_tollfree_admin',
		'type'       => 'text',
  ));

  // Company Fax
  $cmb_front_page->add_field( array(
    'name'       => __( 'Company Fax', 'cmb2' ),
    'default'    => '',
		'id'         => $prefix_front_page . 'company_fax',
		'type'       => 'text',
  ));

    // Use case contact heading
    $cmb_front_page->add_field( array(
      'name'       => __( 'Use Case Contact Us Heading', 'cmb2' ),
      'default'    => '',
      'id'         => $prefix_front_page . 'use_case_contact_heading',
      'type'       => 'text',
    ));
  
    // Use case contact description
    $cmb_front_page->add_field( array(
      'name'       => __( 'Use Case Contact Us Description', 'cmb2' ),
      'default'    => '',
      'id'         => $prefix_front_page . 'use_case_contact_description',
      'type'       => 'textarea',
    ));

    // Use case contact closing
    $cmb_front_page->add_field( array(
      'name'       => __( 'Use Case Contact Us Closing', 'cmb2' ),
      'default'    => '',
      'id'         => $prefix_front_page . 'use_case_contact_closing',
      'type'       => 'textarea',
    ));
}
add_action( 'cmb2_admin_init', 'metaboxes_front_page' );

?>