<?php
/**
 * Displays top navigation in header.php
 */

?>

<nav class="navbar navbar-expand-sm navbar-light main-nav" role="navigation">
    
	<!-- Brand and toggle get grouped for better mobile display -->
    <?php
      $custom_logo_id = get_theme_mod( 'custom_logo' );
      $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    ?>
    
    <!-- Company logo -->
    <a class="navbar-brand" href="<?php echo esc_url(home_url()); ?>">
      <img src="<?php echo $image[0] ?>" class="main-nav__logo" alt="Stratachem">
    </a>
    
    <!-- Toggle for mobile -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-header-menu-container" aria-controls="menu-header-menu-container" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

      <!-- Company contact info -->
      <div class="site-header__contact">
        <div class="site-header__tel"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_phone', true)); ?></div>
        <div class="site-header__email">
          <i class="far fa-envelope"></i>
          <span><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_email', true)); ?></span>
        </div>
      </div>

      <!-- WP nav menu with walker class for Bootstrap collapsable nav menu -->
      <?php
      wp_nav_menu( array(
        'theme_location'    => 'top',
        'depth'             => 2, // 1 = no dropdowns, 2 = with dropdowns.
        'container'         => 'div',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'menu-header-menu-container',
        'menu_class'        => 'nav navbar-nav ml-auto text-right',
        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
        'walker'            => new WP_Bootstrap_Navwalker(),
      ) );
      ?>

</nav>
