<?php
/**
 * The template for displaying the footer
 */
?>

<!-- FOOTER SECTION -->
<footer class="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-md-5 offset-md-1">
          <h4 class="footer__heading">About Us</h4>
          <p><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_footer_about_us', true)); ?></p>
        </div>
        <div class="col-sm-6 col-md-5 offset-md-1 mt-4 mt-sm-0">
          <address>
            <span class="font-weight-bold"><?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_name', true)); ?></span>
            <br> <?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_address_1', true)); ?>
            <br> <?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_address_2', true)); ?>
            <br>
            <br> Office: <?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_office_phone', true)); ?>
            <br> Toll Free Sales: <?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_tollfree_phone', true)); ?>
            <br> Fax: <?php echo esc_html(get_post_meta(get_option('page_on_front'), '_front_page_company_fax', true)); ?>
          </address>
          <a href="<?php echo site_url('/contact-us'); ?>" class="btn btn-outline-light button button--small button--border-green text-small">Send us a message</a>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 offset-md-1">
          <p class="footer__copyright">&copy; Stratachem Solutions <?php the_time('Y'); ?></p>
        </div>
      </div>
    </div>
  </footer>


<?php wp_footer(); ?>
</body>
</html>