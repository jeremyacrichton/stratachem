<?php
/**
 * The template for displaying the header for our theme
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <?php wp_head(); ?>
</head>

<body <?php body_class() ?>>

<!-- HEADER SECTION -->
<header class="pt-4">
  <div class="container">
      <div>

        <!-- MAIN NAVIGATION -->
        <?php if ( has_nav_menu( 'top' ) ) : ?>
          
          <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
          
        <?php endif; ?>

    </div>
  </div>

</header>
