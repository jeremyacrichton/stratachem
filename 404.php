<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header();
?>

<section class="error-404">
  <div class="container">
  <div class="row">
    <div class="col-12">
      <h1 class="heading-primary"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'stratachem' ); ?></h1>
    </div>
    <div class="col-12">
      <a href="<?php echo home_url(); ?>">
        <h4 class="heading-tertiary heading-green">Go Home</h4>
      </a>
    </div>
  </div>
  </div>
</section>
	

<?php
get_footer();
